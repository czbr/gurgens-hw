#!/usr/bin/env sh
echo "Build Finished!"

VER=$(cat VERSION).${BUILD_NUMBER}


mkdir -p reports
echo '{"key":"value"}' > ./reports/result-${VER}.json
